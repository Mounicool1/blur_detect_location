import cv2
import numpy as np
from google.colab.patches import cv2_imshow
import matplotlib.pyplot as plt
import seaborn as sns
image=cv2.imread('/content/72.jpg')
list1=[]
list2=[]
var_blur=[]
var_non=[]
for y in range(0, image.shape[0], 30):
        for x in range(0, image.shape[1], 40):
            patch = image[y:y+30,x:x+40]
            lap1=cv2.Laplacian(image, cv2.CV_64F).var()
            laplacian = cv2.Laplacian(patch, cv2.CV_64F).var()
            #print(laplacian)
            if laplacian < 10:
                 text="blurry"
                 list2.append(text)
                 var_blur.append(laplacian)

            else:
                 text="non_blurry"
                 list1.append(text)
                 var_non.append(laplacian)
ax=sns.distplot(var_non)
ax.set_title('VOL of Non_ Blur Image')
ax.set_xlabel('Variance of Laplacian for Patches')
ax.set_ylabel('No of Patches')