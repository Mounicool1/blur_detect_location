#blending_blur 
blending blur is a function that takes the input paramters like image,x,y (sizes)


#laplacian 
the laplacian is an algorithm that calculates the score of laplacian an image.here it is calculating the score of 30*40(i.e image is divied into 16*16)block.
after that we will compare the score of every block with threshold value of laplacian that we got by plotting the VOL of Image.


#Img2
It is an empty image taken to Select portions of the blur and set blue color channel to 1.0 in img2


#blend
done the linear operation for  blending. Output = image * 0.7 + img2 * 0.3. Output will be in range 0.0 to 1.0.Here the 0.7 and 0.3 are the lpha values for blur and non_blur


#imshow
imshow is the method to to display the output image with the highlighted blur portion with blue color and variance count of image displayed on the out image.


#dataset used
2021-09-22_17_00_00_web.mp4(from s3)