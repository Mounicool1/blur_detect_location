

######Training
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from sklearn.svm import SVC
from functools import reduce
from sklearn.preprocessing import LabelEncoder

dataset = pd.read_csv('/home/einsite/edge_detection_task/csv/train_data/csv_files/merged.csv')
#dataset=dataset.drop('Image',axis='columns')
#dataset

labelencoder = LabelEncoder()
dataset['Text']=labelencoder.fit_transform(dataset['Text'])
X = dataset.iloc[:,:-1]

y = dataset.iloc[:, -1]

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.30,random_state=0)

svc = SVC(kernel='linear',gamma='auto')
svc.fit(X_train, y_train)
validation= svc.predict(X_test)
from sklearn.metrics import f1_score
#f1 score
score=f1_score(y_test, validation)
#print(score)
#from google.colab.patches import cv2_imshow
def blur_non_blur(image, x_slide, y_slide,cap, frame_id):
    #frame_id=0

    #text_b=[]
    #text_non_b=[]
    img_type=[]
    tabel1=[]
    table1=pd.DataFrame(columns=['Image','VOL_b','VOL_non_b','T1_b','T1_non_b'])
    lap1=cv2.Laplacian(image, cv2.CV_64F).var()
    lap1=round(lap1,2)
    score=[]
    for i in range(1,6):
        #print(i)
        
        count=0
        for y in range(0, image.shape[0], y_slide):
            lap_score_non_blurry=[]
            lap_score_b=[]
            lap1_l=[]
            
            for x in range(0, image.shape[1], x_slide):
                patch = image[y:y+y_slide,x:x+x_slide]
                
                
                laplacian = cv2.Laplacian(patch, cv2.CV_64F).var()
                
                if laplacian < i:
                    
                    count=count+1
          
        score.append(count)
    tabel1.append([lap1,score, frame_id])
   
    return img, lap1,tabel1

def predict_image(list):
    #list=[['non_blurry', 510.942126607895, [15, 49, 60, 79, 85], 1]]
    list=reduce(lambda x, y: x.extend(y), list)
    
    temp=list[1]
    del list[-1]
    #t1=temp+list
    l1=[]
    l1.append(list[0])
    l1.append(list[1][0])
    l1.append(list[1][1])
    l1.append(list[1][2])
    l1.append(list[1][3])
    l1.append(list[1][4])
    print(l1)
    
    y_pred=svc.predict([l1])    
    
    for value in y_pred:

        if value==1:
                prediction='non_blurry'
            
        else:
                prediction='blurry'
            
    return prediction

    
cap = cv2.VideoCapture('/home/einsite/edge_detection_task/csv/train_data/2021-08-13_13_10_00_web.mp4')
IMG_SIZE=100
fps=3
frame_id = 1
final_data=[]
result = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
writer = cv2.VideoWriter('/home/einsite/edge_detection_task/csv/train_data/2021-08-13_13_10_00_web.avi', result,
                             3, (640, 480))
while(cap.isOpened()):
    
    ret, frame = cap.read()
    if ret==True:
        print("true")
        img=cv2.resize(frame, (640, 480)) 
   
        
        #for file in files:

        #print(os.path.join(subdir,file))
        #img = cv2.resize(img1, (640,480))
        img ,lap1,tabel1= blur_non_blur(img, 32, 32,img, frame_id)
        #print(tabel1)

        T1_b=tabel1[0][1][0]
        #print(tabel1)
        T2_b=tabel1[0][1][1]
        T3_b=tabel1[0][1][2]
        T4_b=tabel1[0][1][3]
        T5_b=tabel1[0][1][4]
        print(tabel1)
        final_data.append([tabel1])
        prediction=predict_image(tabel1)
        text=str("Frame ID: "+str(frame_id)+"\nVOL: "+str(tabel1[0][0])+"\nT1_b: "+str(T1_b)+"\nT2_b: "+str(T2_b)+"\nT3_b: "+str(T3_b)+"\nT4_b: "+str(T4_b)+"\nT5_b: "+str(T5_b)+"\nSvm_prediction: "+ prediction )
        position = (10, 30)
        font_scale = 0.75
        font = cv2.FONT_HERSHEY_SIMPLEX
        color = (255, 255, 0)
        thickness = 3
        text_size, _ = cv2.getTextSize(text, font, font_scale, thickness)
        line_height = text_size[1] + 5
        line_type = cv2.LINE_AA

        x, y0 = position

        for i, line in enumerate(text.split("\n")):
            y = y0 + i * line_height
            cv2.putText(img,line, (x, y),
                    font,
                    font_scale,
                    color,
                    thickness,
                    line_type)

            #cv2.imshow("img",img)
            #cv2_imshow(blend)

            cv2.imshow("Image",img)
            key = cv2.waitKey(1)
            #print(frame_id)

        
    else:
        break
    frame_id=frame_id+1
    writer.write(img)

cap.release()
writer.release()

#df_1=df_1.to_csv('/home/einsite/edge_detection_task/csv/infer.csv')


 