import csv
import cv2
import pandas as pd
import numpy as np
import os
frame_id=1
def blending_blur(image, x_slide, y_slide,filename):
    img_type, tabel1=[], []
    table1=pd.DataFrame(columns=['Image','VOL_b','VOL_non_b','T1_b','T1_non_b'])
    #img2 = np.zeros((480,640,3))
    lap1=cv2.Laplacian(image, cv2.CV_64F).var()
    score=[]
    for i in range(1,6):
        count=0
        end_time = datetime.now()
        print('Duration: {}'.format(end_time - start_time))
        for y in range(0, image.shape[0], y_slide):
            
            for x in range(0, image.shape[1], x_slide):
                
                patch = np.array(image[y:y+y_slide,x:x+x_slide])
                #lap1=cv2.Laplacian(image, cv2.CV_64F).var()
               
                laplacian = cv2.Laplacian(patch, cv2.CV_64F).var()
                
                if laplacian < i:
                    count=count+1
                    
                    
        score.append(count)
        
    
    list_to_str=str(','.join(str(e) for e in score))
    data=str(round(lap1,2))+","+list_to_str+","
    tabel1.append(str(rootdir)+","+","+str(lap1)+","+data)
    return data

from datetime import datetime
start_time = datetime.now()
print(start_time)
rootdir='/home/einsite/edge_detection_task/csv/train_data/data'
with open('/home/einsite/edge_detection_task/csv/train_data/csv_files/blur2.csv', 'w') as f:
    f.write("VOL,T1,T2,T3,T4,T5,Text\n")
    for subdir,dirs,files in os.walk(rootdir):
        for file in files:
            if "non_blurry" in subdir:
                gt_text="non_blurry"
            else:
                gt_text="blurry"
            img = cv2.imread(os.path.join(subdir,file))
            img = cv2.resize(img, (640,480))
            tabel1= blending_blur(img, 32, 32,os.path.join(subdir,file))
            f.write(str(tabel1)+str(gt_text)+"\n")
            f.flush()
