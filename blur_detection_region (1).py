import cv2
import numpy as np
from google.colab.patches import cv2_imshow
frame_id=1
def blending_blur(image, x_slide, y_slide):
    img2 = np.zeros((480,640,3))
    img = image.copy()
    img1 = image / 255

    for y in range(0, image.shape[0], y_slide):
        for x in range(0, image.shape[1], x_slide):
            patch = image[y:y+y_slide,x:x+x_slide]
            lap1=cv2.Laplacian(img, cv2.CV_64F).var()
            laplacian = cv2.Laplacian(patch, cv2.CV_64F).var()
            #print(laplacian)
            if laplacian < 10:
                  #print("blur")
                  #print(laplacian)
                  img2[y:y+y_slide,x:x+x_slide,0] = img2[y:y+y_slide,x:x+x_slide,0] + 1
#             else:
#                 print('Non Blur')

    blend = cv2.addWeighted(img1,0.7, img2,0.3,0) * 255
    blend = blend.astype(np.uint8)
    return img, img2, blend

img = cv2.imread('/content/57.jpg')
img = cv2.resize(img, (640,480))
img, alpha, blend = blending_blur(img, 8, 8)
cv2.putText(blend,"{}: {:.2f}".format(str(frame_id)+" "+text, lap1), (10, 30),
cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
frame_id=frame_id+1
#cv2_imshow(image)
cv2_imshow(blend)
key = cv2.waitKey(0)
#cv2_imshow(blend)